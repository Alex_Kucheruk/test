package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.testng.Assert

class AccountPage(webDriver: WebDriver?) {

    init {
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = USERNAME_XPATH)
    private val username: WebElement? = null

    @FindBy(xpath = FIRST_NAME_XPATH)
    private val firstName: WebElement? = null

    @FindBy(xpath = LAST_NAME_XPATH)
    private val lastName: WebElement? = null

    @FindBy(xpath = EMAIL_XPATH)
    private val email: WebElement? = null

    @FindBy(xpath = PHONE_XPATH)
    private val phone: WebElement? = null

    @FindBy(xpath = ADDRESS_1_XPATH)
    private val address1: WebElement? = null

    @FindBy(xpath = ADDRESS_2_XPATH)
    private val address2: WebElement? = null

    @FindBy(xpath = CITY_XPATH)
    private val city: WebElement? = null

    @FindBy(xpath = STATE_XPATH)
    private val state: WebElement? = null

    @FindBy(xpath = ZIP_XPATH)
    private val zip: WebElement? = null

    @FindBy(xpath = COUNTRY_XPATH)
    private val country: WebElement? = null

    fun checkUsername(usernameString: String) {
        Assert.assertEquals(username?.text, usernameString)
    }

    fun checkFirstName(firsNameString: String) {
        Assert.assertEquals(firstName?.getAttribute(VALUE_ATTRIBUTE), firsNameString)
    }

    fun checkLastName(lastNameString: String) {
        Assert.assertEquals(lastName?.getAttribute(VALUE_ATTRIBUTE), lastNameString)
    }

    fun checkEmail(emailString: String) {
        Assert.assertEquals(email?.getAttribute(VALUE_ATTRIBUTE), emailString)
    }

    fun checkPhone(phoneString: String) {
        Assert.assertEquals(phone?.getAttribute(VALUE_ATTRIBUTE), phoneString)
    }

    fun checkAddress1(addressString: String) {
        Assert.assertEquals(address1?.getAttribute(VALUE_ATTRIBUTE), addressString)
    }

    fun checkAddress2(addressString: String) {
        Assert.assertEquals(address2?.getAttribute(VALUE_ATTRIBUTE), addressString)
    }

    fun checkCity(cityString: String) {
        Assert.assertEquals(city?.getAttribute(VALUE_ATTRIBUTE), cityString)
    }

    fun checkState(stateString: String) {
        Assert.assertEquals(state?.getAttribute(VALUE_ATTRIBUTE), stateString)
    }

    fun checkZip(zipString: String) {
        Assert.assertEquals(zip?.getAttribute(VALUE_ATTRIBUTE), zipString)
    }

    fun checkCountry(countryString: String) {
        Assert.assertEquals(country?.getAttribute(VALUE_ATTRIBUTE), countryString)
    }


    companion object {
        private const val VALUE_ATTRIBUTE = "value"
        private const val USERNAME_XPATH = "//*[@id=\"Catalog\"]/form/table[1]/tbody/tr[1]/td[2]"
        private const val FIRST_NAME_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[1]/td[2]/input"
        private const val LAST_NAME_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[2]/td[2]/input"
        private const val EMAIL_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[3]/td[2]/input"
        private const val PHONE_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[4]/td[2]/input"
        private const val ADDRESS_1_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[5]/td[2]/input"
        private const val ADDRESS_2_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[6]/td[2]/input"
        private const val CITY_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[7]/td[2]/input"
        private const val STATE_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[8]/td[2]/input"
        private const val ZIP_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[9]/td[2]/input"
        private const val COUNTRY_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[10]/td[2]/input"
    }
}