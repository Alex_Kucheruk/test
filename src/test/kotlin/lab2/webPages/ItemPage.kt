package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class ItemPage(webDriver: WebDriver?) {

    init{
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = ADD_TO_XPATH)
    private val item : WebElement? = null

    fun clickOnItem() {
        item?.click()
    }


    companion object {
        private const val ADD_TO_XPATH = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a"
    }
}