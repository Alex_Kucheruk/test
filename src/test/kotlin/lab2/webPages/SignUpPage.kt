package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class SignUpPage(webDriver: WebDriver?) {

    init {
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = USERNAME_XPATH)
    private val username: WebElement? = null

    @FindBy(xpath = PASSWORD_XPATH)
    private val password: WebElement? = null

    @FindBy(xpath = PASSWORD_REPEAT_XPATH)
    private val repeatPassword: WebElement? = null

    @FindBy(xpath = FIRST_NAME_XPATH)
    private val firstName: WebElement? = null

    @FindBy(xpath = LAST_NAME_XPATH)
    private val lastName: WebElement? = null

    @FindBy(xpath = EMAIL_XPATH)
    private val email: WebElement? = null

    @FindBy(xpath = PHONE_XPATH)
    private val phone: WebElement? = null

    @FindBy(xpath = ADDRESS_1_XPATH)
    private val address1: WebElement? = null

    @FindBy(xpath = ADDRESS_2_XPATH)
    private val address2: WebElement? = null

    @FindBy(xpath = CITY_XPATH)
    private val city: WebElement? = null

    @FindBy(xpath = STATE_XPATH)
    private val state: WebElement? = null

    @FindBy(xpath = ZIP_XPATH)
    private val zip: WebElement? = null

    @FindBy(xpath = COUNTRY_XPATH)
    private val country: WebElement? = null

    @FindBy(xpath = SIGN_UP_XPATH)
    private val signUpButton: WebElement? = null

    fun setUsername(usernameString: String) {
        username?.clear()
        username?.sendKeys(usernameString)
    }

    fun setPassword(passwordString: String) {
        password?.clear()
        password?.sendKeys(passwordString)
    }

    fun setPasswordRepeat(passwordConfirmString: String) {
        repeatPassword?.clear()
        repeatPassword?.sendKeys(passwordConfirmString)
    }

    fun setFirstName(firsNameString: String) {
        firstName?.clear()
        firstName?.sendKeys(firsNameString)
    }

    fun setLastName(lastNameString: String) {
        lastName?.clear()
        lastName?.sendKeys(lastNameString)
    }

    fun setEmail(emailString: String) {
        email?.clear()
        email?.sendKeys(emailString)
    }

    fun setPhone(phoneString: String) {
        phone?.clear()
        phone?.sendKeys(phoneString)
    }

    fun setAddress1(addressString: String) {
        address1?.clear()
        address1?.sendKeys(addressString)
    }

    fun setAddress2(addressString: String) {
        address2?.clear()
        address2?.sendKeys(addressString)
    }

    fun setCity(cityString: String) {
        city?.clear()
        city?.sendKeys(cityString)
    }

    fun setState(stateString: String) {
        state?.clear()
        state?.sendKeys(stateString)
    }

    fun setZip(zipString: String) {
        zip?.clear()
        zip?.sendKeys(zipString)
    }

    fun setCountry(countryString: String) {
        country?.clear()
        country?.sendKeys(countryString)
    }

    fun clickOnSignUp() {
        signUpButton?.click()
    }

    companion object {
        private const val USERNAME_XPATH = "/html/body/div[2]/div/form/table[1]/tbody/tr[1]/td[2]/input"
        private const val PASSWORD_XPATH = "//*[@id=\"Catalog\"]/form/table[1]/tbody/tr[2]/td[2]/input"
        private const val PASSWORD_REPEAT_XPATH = "//*[@id=\"Catalog\"]/form/table[1]/tbody/tr[3]/td[2]/input"
        private const val FIRST_NAME_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[1]/td[2]/input"
        private const val LAST_NAME_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[2]/td[2]/input"
        private const val EMAIL_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[3]/td[2]/input"
        private const val PHONE_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[4]/td[2]/input"
        private const val ADDRESS_1_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[5]/td[2]/input"
        private const val ADDRESS_2_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[6]/td[2]/input"
        private const val CITY_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[7]/td[2]/input"
        private const val STATE_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[8]/td[2]/input"
        private const val ZIP_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[9]/td[2]/input"
        private const val COUNTRY_XPATH = "//*[@id=\"Catalog\"]/form/table[2]/tbody/tr[10]/td[2]/input"
        private const val SIGN_UP_XPATH = "//*[@id=\"Catalog\"]/form/input"
    }
}