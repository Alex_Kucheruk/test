package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class SignInPage(webDriver: WebDriver?) {

    init {
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = USERNAME_XPATH)
    private val user : WebElement? = null

    @FindBy(xpath = PASSWORD_XPATH)
    private val password : WebElement? = null

    @FindBy(xpath = LOGIN_BUTTON_XPATH)
    private val loginButton : WebElement? = null

    @FindBy(xpath = SIGN_UP_XPATH)
    private val signUp : WebElement? = null

    @FindBy(xpath = INCORRECT_CREDS_PATH)
    private val incorrectCreds : WebElement? = null

    fun setUsername(usernameString : String) {
        user?.clear()
        user?.sendKeys(usernameString)
    }

    fun setPassword(passwordString : String) {
        password?.clear()
        password?.sendKeys(passwordString)
    }

    fun signIn() {
        loginButton?.click()
    }

    fun isIncorrectCreds() : Boolean {
        return incorrectCreds?.text?.contains(INCORRECT_CREDS_TEXT) ?: false
    }

    fun clickOnSignUp() {
        signUp?.click()
    }

    companion object {
        private const val USERNAME_XPATH = "/html/body/div[2]/div/form/p[2]/input[1]"
        private const val PASSWORD_XPATH = "//*[@id=\"Catalog\"]/form/p[2]/input[2]"
        private const val LOGIN_BUTTON_XPATH = "//*[@id=\"Catalog\"]/form/input"
        private const val SIGN_UP_XPATH = "//*[@id=\"Catalog\"]/a"
        private const val INCORRECT_CREDS_PATH = "//*[@id=\"Content\"]/ul/li"
        private const val INCORRECT_CREDS_TEXT = "Invalid username or password. Signon failed."
    }
}