package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class MyOrdersPage(webDriver: WebDriver?) {

    init {
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = ITEM_XPATH)
    private val item : WebElement? = null

    @FindBy(xpath = PRICE_XPATH)
    private val price : WebElement? = null

    fun removeItem() {
        item?.click()
    }

    fun isZeroPrice() : Boolean {
        println(price?.text)
        return price?.text?.contains(PRICE_TEXT) ?: false
    }

    companion object {
        private const val ITEM_XPATH = "//*[@id=\"Cart\"]/form/table/tbody/tr[2]/td[8]/a"
        private const val PRICE_XPATH = "//*[@id=\"Cart\"]/form/table/tbody/tr[3]/td[1]"
        private const val PRICE_TEXT = "Sub Total: \$0.00"
    }
}