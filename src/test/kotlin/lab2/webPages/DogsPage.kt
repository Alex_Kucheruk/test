package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class DogsPage(webDriver: WebDriver?) {

    init {
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = ITEM_XPATH)
    private val item : WebElement? = null

    fun clickOnItem() {
        item?.click()
    }

    companion object {
        private const val ITEM_XPATH = "//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[1]/a"
    }
}