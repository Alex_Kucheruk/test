package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class HomePage(webDriver: WebDriver?) {

    init {
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = SIGN_IN_OUT_XPATH)
    private val signInOutButton : WebElement? = null

    @FindBy(xpath = DOGS_XPATH)
    private val dogsLink : WebElement? = null

    @FindBy(xpath = MY_ACCOUNT_XPATH)
    private val myAccountLink : WebElement? = null

    fun clickOnSignIn() {
        signInOutButton?.click()
    }

    fun clickOnSignOut() {
        signInOutButton?.click()
    }

    fun clickOnDogs() {
        dogsLink?.click()
    }

    fun clickOnMyAccount() {
        myAccountLink?.click()
    }


    companion object {
        private const val SIGN_IN_OUT_XPATH = "//*[@id=\"MenuContent\"]/a[2]"
        private const val DOGS_XPATH = "//*[@id=\"QuickLinks\"]/a[2]"
        private const val MY_ACCOUNT_XPATH = "//*[@id=\"MenuContent\"]/a[3]"
    }

}