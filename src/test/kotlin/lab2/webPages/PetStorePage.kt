package lab2.webPages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory


class PetStorePage(webDriver: WebDriver?) {

    init {
        webDriver?.get(PET_STORE_URL)
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(xpath = CONTENT_XPATH)
    private val contentButton : WebElement? = null

    fun clickOnContent() {
        contentButton?.click()
    }


    companion object {
        private const val PET_STORE_URL = "https://petstore.octoperf.com/"
        private const val CONTENT_XPATH = "//*[@id=\"Content\"]/p[1]/a"
    }
}