package lab2.tests


import lab2.webPages.*
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.testng.Assert
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test

class Test2 {

    private var driver: WebDriver? = null
    private var petStorePage: PetStorePage? = null
    private var homePage: HomePage? = null
    private var signInPage: SignInPage? = null
    private var dogsPage: DogsPage? = null
    private var itemPage: ItemPage? = null
    private var myOrdersPage: MyOrdersPage? = null
    private var signUpPage: SignUpPage? = null
    private var accountPage: AccountPage? = null

    @BeforeTest
    fun createDriver() {
        System.setProperty(WEB_DRIVER_KEY, WEB_DRIVER_EXE)
        driver = ChromeDriver()
        driver?.apply {
            manage().window().maximize()
        }
        petStorePage = PetStorePage(driver)
        homePage = HomePage(driver)
        signInPage = SignInPage(driver)
        dogsPage = DogsPage(driver)
        itemPage = ItemPage(driver)
        myOrdersPage = MyOrdersPage(driver)
        signUpPage = SignUpPage(driver)
        accountPage = AccountPage(driver)
    }

    @Test
    fun logInLogOutTest() {
        petStorePage?.clickOnContent()

        homePage?.clickOnSignIn()

        signInPage?.apply {
            setUsername(CORRECT_USERNAME)
            setPassword(PASSWORD)
            signIn()
        }

        homePage?.clickOnSignOut()
    }

    @Test
    fun registrationTest() {
        petStorePage?.clickOnContent()

        homePage?.clickOnSignIn()

        signInPage?.clickOnSignUp()

        signUpPage?.apply {
            setUsername(CORRECT_USERNAME)
            setPassword(PASSWORD)
            setPasswordRepeat(PASSWORD)
            setFirstName(FIRST_NAME)
            setLastName(LAST_NAME)
            setEmail(EMAIL)
            setPhone(PHONE)
            setAddress1(ADDRESS_1)
            setAddress2(ADDRESS_2)
            setCity(CITY)
            setState(STATE)
            setZip(ZIP)
            setCountry(COUNTRY)
            clickOnSignUp()
        }

        homePage?.clickOnSignIn()

        signInPage?.apply {
            setUsername(CORRECT_USERNAME)
            setPassword(PASSWORD)
            signIn()
        }

        homePage?.clickOnMyAccount()

        accountPage?.apply {
            checkAddress1(ADDRESS_1)
            checkAddress2(ADDRESS_2)
            checkCity(CITY)
            checkCountry(COUNTRY)
            checkEmail(EMAIL)
            checkFirstName(FIRST_NAME)
            checkLastName(LAST_NAME)
            checkPhone(PHONE)
            checkZip(ZIP)
            checkState(STATE)
            checkUsername(CORRECT_USERNAME)
        }
    }

    @Test
    fun positiveLogInTest() {
        petStorePage?.clickOnContent()

        homePage?.clickOnSignIn()

        signInPage?.apply {
            setUsername(CORRECT_USERNAME)
            setPassword(PASSWORD)
            signIn()
        }
    }

    @Test
    fun addRemoveItemTest() {
        petStorePage?.clickOnContent()

        homePage?.clickOnSignIn()

        signInPage?.apply {
            setUsername(CORRECT_USERNAME)
            setPassword(PASSWORD)
            signIn()
        }

        homePage?.clickOnDogs()

        dogsPage?.clickOnItem()

        itemPage?.clickOnItem()

        Assert.assertEquals(myOrdersPage?.isZeroPrice(), false)

        myOrdersPage?.removeItem()

        Assert.assertEquals(myOrdersPage?.isZeroPrice(), true)
    }

    @Test
    fun negativeLogInTest() {
        petStorePage?.clickOnContent()

        homePage?.clickOnSignIn()

        signInPage?.apply {
            setUsername(INCORRECT_USERNAME)
            setPassword(PASSWORD)
            signIn()
            Assert.assertEquals(isIncorrectCreds(), true)
        }

    }


    companion object {
        private const val WEB_DRIVER_KEY = "webdriver.chrome.driver"
        private const val WEB_DRIVER_EXE = "src\\main\\kotlin\\drivers\\chromedriver.exe"
        private const val PASSWORD = "password1"
        private const val CORRECT_USERNAME = "yuyuk"
        private const val INCORRECT_USERNAME = "127"
        private const val FIRST_NAME = "Alex"
        private const val LAST_NAME = "Kucheruk"
        private const val EMAIL = "sdvefbfdv@gmail.com"
        private const val PHONE = "0989934505"
        private const val ADDRESS_1 = "central 67"
        private const val ADDRESS_2 = "new 88"
        private const val CITY = "Dnipro"
        private const val STATE = "5"
        private const val ZIP = "45674"
        private const val COUNTRY = "Ukraine"
    }
}