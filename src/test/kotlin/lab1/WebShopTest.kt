package lab1

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.Select
import org.testng.Assert
import org.testng.annotations.AfterTest
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test

class WebShopTest {

    var driver: WebDriver? = null

    @BeforeTest
    fun createDriver() {
        System.setProperty(WEB_DRIVER_KEY, WEB_DRIVER_EXE)
        driver = ChromeDriver()
        driver?.apply {
            manage().window().maximize()
        }

    }

    @AfterTest
    fun closeDriver() {
        driver?.quit()
    }

    @Test
    fun openWebPageTest() {
        driver?.apply {
            get(WEB_SHOP_URL)
            Assert.assertEquals(currentUrl, WEB_SHOP_URL)
            Thread.sleep(SLEEPING_TIME)
            findElement(By.xpath(APPAREL_LINK)).click()
            Thread.sleep(SLEEPING_TIME)

            val orderByOptionSelect = findElement(By.id(ORDER_BY_ID))
            val optionListOrderBy = Select(orderByOptionSelect)
            optionListOrderBy.selectByIndex(2)
            Thread.sleep(SLEEPING_TIME)

            val viewType = findElement(By.id(VIEW_TYPE_ID))
            val optionList = Select(viewType)
            optionList.selectByIndex(1)
            Thread.sleep(SLEEPING_TIME)


            val shopList: List<WebElement> = findElements(By.cssSelector(".product-item"))
            println(shopList.size)
            var i = 0
            for (shop in shopList) {
                Thread.sleep(SLEEPING_TIME)
                //TODO stale element reference: element is not attached to the page document
                val itemUrl = shop.findElement(By.xpath("//div[@class='picture']/a"))
                itemUrl.click()
                Thread.sleep(SLEEPING_TIME)
                val addToList = findElement(By.cssSelector(".button-1.add-to-cart-button"))
                addToList.click()
                navigate().back()
                i++
                Thread.sleep(SLEEPING_TIME)
                if (i == 2) break
            }
            navigate().refresh()
            val shoppingCard = findElement(By.cssSelector(SHOPPING_CARDS_CLASS))
            Assert.assertEquals(shoppingCard.text, SHOPPING_CARDS_TEXT)

        }
    }

    companion object {
        private const val WEB_DRIVER_KEY = "webdriver.chrome.driver"
        private const val WEB_DRIVER_EXE = "src\\main\\kotlin\\drivers\\chromedriver.exe"
        private const val WEB_SHOP_URL = "http://demowebshop.tricentis.com/"
        private const val APPAREL_LINK = "//a[@href ='/apparel-shoes']"
        private const val ORDER_BY_ID = "products-orderby"
        private const val VIEW_TYPE_ID = "products-viewmode"
        private const val SHOPPING_CARDS_CLASS = ".cart-qty"
        private const val SHOPPING_CARDS_TEXT = "(2)"
        private const val SLEEPING_TIME = 2000L
    }
}