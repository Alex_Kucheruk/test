package lab1

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.Select
import org.testng.Assert
import org.testng.annotations.AfterTest
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test


class PastebinTest {

    var driver: WebDriver? = null

    @BeforeTest
    fun createDriver() {
        System.setProperty(WEB_DRIVER_KEY, WEB_DRIVER_EXE)
        driver = ChromeDriver()
        driver?.apply {
            manage().window().maximize()
        }
    }



    @Test
    fun openWebPageTest() {
        driver?.apply {
            get(PASTEBIN_URL)
            Assert.assertEquals(currentUrl, PASTEBIN_URL)

            val newPasteElement: WebElement = findElement(By.id(NEW_PASTE_ID))
            newPasteElement.sendKeys(INPUT_TEXT)
            Assert.assertEquals(newPasteElement.getAttribute(VALUE_ATTRIBUTE), INPUT_TEXT)
            Thread.sleep(SLEEPING_TIME)

            findElement(By.id(EXPIRATION_CONTAINER_ID)).click()
            Thread.sleep(SLEEPING_TIME)
            findElement(By.id(EXPIRATION_CONTAINER_ID)).findElement(By.xpath(SELECTED_PASTE_EXPIRATION_OPTION_VALUE)).click()
            Thread.sleep(SLEEPING_TIME)

            val pasteTitleElement: WebElement = findElement(By.id(PASTE_TITLE_ID))
            pasteTitleElement.sendKeys(INPUT_TEXT)
            Assert.assertEquals(pasteTitleElement.getAttribute(VALUE_ATTRIBUTE), INPUT_TEXT)
            Thread.sleep(SLEEPING_TIME)

            findElement(By.xpath(NEW_PASTE_BUTTON_XPATH)).click()
            Thread.sleep(SLEEPING_TIME)
            Assert.assertEquals(findElement(By.xpath(INFO_TOP_XPATH)).text, INPUT_TEXT)
            Assert.assertEquals(findElement(By.xpath(CONTAINER_XPATH)).text, INPUT_TEXT)
            Assert.assertEquals(findElement(By.xpath(TEXT_AREA_XPATH)).text, INPUT_TEXT)
        }
    }


    companion object {
        private const val WEB_DRIVER_KEY = "webdriver.chrome.driver"
        private const val WEB_DRIVER_EXE = "src\\main\\kotlin\\drivers\\chromedriver.exe"
        private const val PASTEBIN_URL = "https://pastebin.com/"
        private const val VALUE_ATTRIBUTE = "value"
        private const val NEW_PASTE_ID = "postform-text"
        private const val PASTE_TITLE_ID = "postform-name"
        private const val EXPIRATION_CONTAINER_ID = "select2-postform-expiration-container"
        private const val SELECTED_PASTE_EXPIRATION_OPTION_VALUE = "//li[text()='10 Minutes']"
        private const val NEW_PASTE_BUTTON_XPATH = "//*[@id=\"w0\"]/div[5]/div[1]/div[8]/button"
        private const val INFO_TOP_XPATH = "//div[@class='info-top']"
        private const val CONTAINER_XPATH = "//div[@class='de1']"
        private const val TEXT_AREA_XPATH = "//textarea[@class='textarea']"
        private const val INPUT_TEXT = "будь-який текст"
        private const val SLEEPING_TIME = 2000L


    }
}

